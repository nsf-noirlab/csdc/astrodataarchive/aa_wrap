# EXAMPLE:
#   pushd ~/sandbox/aa_wrap; python -m pytest; popd
#   python -m pytest tests/test_helpers_api.py
#   report execution times of 3 slowest tests over 9 seconds
#   python -m pytest --durations=3 --durations-min=9.0 tests/test_helpers_api.py  
#   python -m pytest tests/test_helpers_api.py::test_find_4   # run one test
#   python -m pytest -rs tests  # report skipped tests

# Python Standard Library
from pprint import pformat
from pathlib import Path, PosixPath
import tempfile
# Local Packages
import helpers.api
# External Packages
import pytest


rooturl = 'https://astroarchive.noao.edu/' #@@@
#rooturl = 'https://marsnat1.pat.dm.noao.edu/' #@@@

fapi = helpers.api.FitsFile(rooturl, verbose=False, limit=5)
spec1 = { "outfields": ["md5sum"] }  # Minimum spec

def test_using_prod():
    assert rooturl == 'https://astroarchive.noao.edu/'
    
def test_fapi():
    assert fapi.limit == 5
    pass
    
def test_version():
    assert fapi.version == 5.0
    
def test_search():
    """Search works, but warns of deprecation"""
    with pytest.deprecated_call():
        info,rows = fapi.search({"outfields": ["archive_filename"],
                                 "search":[]})
    assert len(rows) == 5

def test_search_csv():
    info,rows = fapi.search({"outfields": ["md5sum","caldat"], "search":[]},
                         format='csv')
    #rows: b'caldat,md5sum\r\n2011-04-07,0000004ab27d9e427bb93c640b358633\r\n2014-01-21,0000041587917c9cb234c6116b396394\r\n2015-07-23,0000065ee227de201686b54706ba58e9\r\n2016-10-17,0000079ed58542c36a059467cc748420\r\n2017-06-21,000007c08dc11d70622574eec3819a02\r\n'
    assert len(rows.decode().split('\r\n')) == (fapi.limit + 2)
    
aa = helpers.api.AdaApi(rooturl, verbose=True, limit=5)

def test_find_1():
    """Generalized file/hdu search"""
    jdata = {"outfields": ["md5sum"], "search":[]}
    info, rows = aa.find(jdata, rectype='file')
    assert len(rows) == 5

def test_find_2():
    """Find files"""
    spec2 = {  
        "outfields": ["md5sum", "archive_filename"],
        "search": [["instrument", "decam"]]}
    info, rows = aa.find(spec2, rectype='file')
    assert len(rows) == 5

@pytest.mark.skip(reason="Something wrong. Takes too long!!!")    
def test_find_3():
    """Find HDUs"""
    aa = helpers.api.AdaApi(rooturl, verbose=False, limit=5)

    # Only works with rectype='hdu'
    spec3 = {  
        "outfields": ["exposure", "ifilter", "AIRMASS",
                      "hdu:ra_min", "hdu:ra_max", "hdu:dec_min", "hdu:dec_max",
                      "hdu:FWHM", "hdu:AVSKY"],
        "search": [["hdu:ra_center", -400, 400],
                   ["instrument", "decam"],
                   ["proc_type", "instcal"]]}
    info, rows = aa.find(spec3, rectype='hdu')
    assert len(rows) == 5

def test_find_4():
    """Invalid search spec. Say what is wrong with spec."""
    try:
        bad_spec = {'foobar': 99}
        info, rows = aa.find(bad_spec, rectype='file')
    except Exception as err:
        print(f'Exception using find: {bad_spec}):{err}')
        expected = "400 Client Error: Bad Request for url: https://astroarchive.noao.edu/api/adv_search/find/?rectype=file&limit=5&format=json"
        assert expected, err
    else:
        assert False, "Did not get expected exception"


def test_vosearch():
    lim = 2
    info,rows = fapi.vosearch(13, -34, 1, limit=lim, format='json')
    print(f'len(rows)={len(rows)} info={info}')
    assert len(rows) == lim
    assert info['RESULTS']['MORE']  # we didn't get them all

    
# SLOW!!! @@@
#! @pytest.mark.skip(reason="Mass Storage is DOWN")
def test_retrieve_proprietary():
    proprietaryFid = 'a96e55509a4cf89ebcc3126bef2e6aa7' # from S&F
    fapi_auth = helpers.api.FitsFile(rooturl,
                                     email="demo_user@mail.edu",
                                     password="!Password")
    fapi_auth = helpers.api.FitsFile('https://astroarchive.noao.edu/', email="demo_user@mail.edu", password="!Password")

    #!local_file_path = (f'~/Downloads/noirlab/fits_proprietary_{proprietaryFid}')
    #!fullpath = fapi_auth.retrieve(proprietaryFid, local_file_path)
    #!assert fullpath.stat().st_size == 313600320
    fileobj,fullpath = tempfile.mkstemp()
    ok = fapi_auth.retrieve(proprietaryFid, fileobj)
    assert PosixPath(fullpath).stat().st_size == 313600320

#@pytest.mark.skip(reason="Mass Storage is DOWN")
def test_retrieve_public():
    publicFid = '0000298c7e0b3ce96b3fff51515a6100'
    #!local_file_path = (f'~/Downloads/noirlab/fits_public_{publicFid}')
    #!fullpath = fapi.retrieve(publicFid, local_file_path)
    fileobj,fullpath = tempfile.mkstemp()
    ok = fapi.retrieve(publicFid, fileobj)
    assert PosixPath(fullpath).stat().st_size == 74914560

