Changelog
=========

All notable changes to this project will be documented in this file.

The format is based on `Keep a Changelog <https://keepachangelog.com/en/1.0.0/>`_,
and this project adheres to
`Semantic Versioning <https://semver.org/spec/v2.0.0.html>`_.



[Unreleased]
------------

Added
~~~~~

- find :: Get File or Hdu records based on JSON search spec
- search :: DEPRECATED version of "find"
- vosearch :: Find File or Hdu records using SIA and returning VOTABLE
- retrieve :: Download a FITS file given its ID (md5sum)

- contrib/download_decam_expnum.py :: Download a set of DECAM FITS files
  containing given EXPNUM values.
- contrib/exposure_map.py :: Create an exposure map from Archive metadata
- contrib/exposure_map_v2.py ::
- contrib/night_files.py :: Get list of all files for specific
  telescope,instrument,night.
  
