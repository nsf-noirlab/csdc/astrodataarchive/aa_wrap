.. AAWrap documentation master file, created by
   sphinx-quickstart on Wed Jan 20 14:23:40 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to AAWrap's documentation!
==================================

.. automodule:: helpers.api
    :members:

.. COMMENT automodule:: 
    :members:

##############################################################################

.. toctree::
   :caption: Contrib
   :maxdepth: 2
   :glob:

   contrib/*


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
