
.. _download_decam_expnum:

##############################################################################

Download a set of DECAM FITS files containing given EXPNUM values.
------------------------------------------------------------------

.. argparse::
   :module: helpers.contrib.download_decam_expnum
   :func: dde_parser
   :prog: download_decam_expnum

          
