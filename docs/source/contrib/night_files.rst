
.. _night_files:

##############################################################################


Get list of all files for a specific telescope,instrument,night.
----------------------------------------------------------------

.. argparse::
   :module: helpers.contrib.night_files
   :func: nf_parser
   :prog: night_files

