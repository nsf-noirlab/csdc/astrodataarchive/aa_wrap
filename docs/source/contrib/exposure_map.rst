
.. _exposure_map:

##############################################################################

Make an Exposure Map from Archive metadata.
--------------------------------------------

.. argparse::
   :module: helpers.contrib.exposure_map_v2
   :func: em_parser
   :prog: exposure_map_v2
